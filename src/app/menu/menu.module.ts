import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IntroComponent } from './intro/intro.component';
import { TodoComponent } from './todo/todo.component';
import { TodoItemComponent } from './todo/todoItem/todoItem.component';
import { FormsModule } from '@angular/forms';
import { AddTodoComponent } from './todo/add-todo/add-todo.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
  ],
  declarations: [IntroComponent, TodoComponent, TodoItemComponent, AddTodoComponent],
  exports: [IntroComponent, TodoComponent]
})
export class MenuModule { }
