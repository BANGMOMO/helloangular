import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-add-todo',
  template: `
    <input type="text" placeholder="할 일 추가" [(ngModel)]="newTxt">
    <button (click)="addTxt(newTxt)">추가</button>
  `,
  styles: []
})
export class AddTodoComponent implements OnInit {
  newTxt: string;

  @Output() onTodoAdded = new EventEmitter();
  constructor() {
  }

  ngOnInit() {
  }

  addTxt(newTxt: string) {
    this.onTodoAdded.emit(newTxt);
    this.newTxt = '';
  }
}
