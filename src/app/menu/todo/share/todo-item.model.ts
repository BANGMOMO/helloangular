export class TodoItem {
  done: boolean;
  text: string;
}
