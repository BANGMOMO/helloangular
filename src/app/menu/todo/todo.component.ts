import { Component, OnInit } from '@angular/core';
import {TodoItem} from './share/todo-item.model';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  newTxt: string;
  todos: TodoItem[];
  modifyTxt: string;

  constructor() {
    this.newTxt = '';
    this.todos = [
      {done: false, text: '운동하기'},
      {done: true, text: '운동하기2'}
    ];
  }

  ngOnInit() {
  }

  toggleTodo(todo) {
    todo.done = !todo.done;
  }

  addTodo(newTxt: string) {
    this.todos.push({
      done: false,
      text: newTxt
    });
  }

  removeTodo(idx: number) {
    this.todos.splice(idx, 1);
  }

  modifyTodo(obj) {
    this.todos[obj.idx].text = obj.modTxt;
  }
}
