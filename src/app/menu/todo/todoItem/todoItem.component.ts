import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {TodoItem} from '../share/todo-item.model';
@Component({
  selector: 'app-todo-item',
  template: `
    <div class='{{ clsName }}'>
      <input type="checkbox" [checked]="todo.done">{{ todo.text }}
      <button (click)="remove(idx)">삭제</button>
      <button (click)="modify(idx)">수정</button>
      <div class="modyInput">
        <input type="text" [(ngModel)]="modTxt">
        <button (click)="save(idx, modTxt)">저장</button>
      </div>
    </div>
  `,
  styles: [`
    :host > div {
      padding: 10px 0;
    }
    :host .modyInput {
      display: none;
    }
    :host > div.active .modyInput {
      display: block;
    }
  `]
})
export class TodoItemComponent implements OnInit, OnChanges {
  clsName: string;
  modTxt: string;

  @Input() todo: TodoItem;
  @Input() idx: number;

  @Output() removeEl = new EventEmitter();
  @Output() modifyEl = new EventEmitter();

  constructor() {  }

  ngOnInit() {
  }

  ngOnChanges() {
    this.modTxt = this.todo.text;
  }

  remove(idx: number) {
    this.removeEl.emit(idx);
  }

  modify(idx: number) {
    this.modTxt = this.todo.text;

    if (this.clsName === 'active') {
      this.clsName = '';
    } else {
      this.clsName = 'active';
    }
  }

  save(idx: number, modTxt: string) {
    this.modifyEl.emit({
      idx: idx,
      modTxt: modTxt
    });
    this.modTxt = '';
    this.clsName = '';
  }
}
