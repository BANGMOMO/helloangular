import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.css']
})
export class IntroComponent implements OnInit {

  JSON_DATA = [
    {
      'id': 1,
      'name': '개미',
      'phone': '010-0001-0001',
      'address1': '허리도 가늘군 만지면 부서지리',
      'address2': '111-12'
    }, {
      'id': 2,
      'name': '배짱이',
      'phone': '010-0002-0002',
      'address1': '가시 동구 동동',
      'address2': '333-10 1004호'
    }, {
      'id': 3,
      'name': '사자',
      'phone': '010-4444-3333',
      'address1': '무시 상구 아리아리동동',
      'address2': '323-12 44호'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
