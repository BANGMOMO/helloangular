import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import {MenuModule} from './menu/menu.module';
import {IntroComponent} from './menu/intro/intro.component';
import {TodoComponent} from './menu/todo/todo.component';

const appRoutes: Routes = [
  { path: 'intro', component: IntroComponent },
  { path: 'todo', component: TodoComponent },
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
    ),
    BrowserModule,
    MenuModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
